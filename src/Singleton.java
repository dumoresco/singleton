public class Singleton {

    // Private attribute
    private static Singleton uniqueInstance;

    // Private constructor
    private Singleton(){

    }

    // Synchronized to prevent more than one thread from being able to access it
    public static synchronized Singleton getInstance(){
        if(uniqueInstance == null){
            uniqueInstance = new Singleton();
        }
        return uniqueInstance;
    };

}
